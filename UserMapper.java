package com.empirefree.huyuqiao.mapper;

/**
 * @program: springboot-sharding
 * @description: mapper接口
 * @author: huyuqiao
 * @create: 2021/06/19 16:19
 */


import com.empirefree.huyuqiao.domain.UserDO;

import java.util.List;

public interface UserMapper {
    //查询
    List<UserDO> all();

    //添加
    Integer add(UserDO userDO);

    //更新
    Integer update(UserDO userDO);

    //删除
    Integer delete(String id);
}
