package com.empirefree.huyuqiao.controller;

/**
 * @program: springboot-sharding
 * @description: controller
 * @author: huyuqiao
 * @create: 2021/06/19 16:23
 */

import com.empirefree.huyuqiao.domain.UserDO;
import com.empirefree.huyuqiao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("all")
    public Object all() {
        return userService.all();
    }

    @PostMapping("allMybatisPlus")
    public Object allMybatisPlus() {
        return userService.allMybatisPlus();
    }

    @PostMapping("add")
    public Object add(@RequestBody UserDO userDO) {
        return userService.add(userDO);
    }

    @PostMapping("update")
    public Object update(@RequestBody UserDO userDO) {
        return userService.update(userDO);
    }

    @GetMapping("delete")
    public Object delete(@RequestParam("id") String id) {
        return userService.delete(id);
    }
}
