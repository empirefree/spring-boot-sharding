package com.empirefree.huyuqiao.domain;

/**
 * @program: springboot-sharding
 * @description: 实体类
 * @author: huyuqiao
 * @create: 2021/06/19 16:17
 */

import java.util.Date;

public class UserDO {
    private String id;
    private String userName;
    private Integer age;
    private Date createTime;
    private Integer tags;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getTags() {
        return tags;
    }

    public void setTags(Integer tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "UserDO{" +
                "id='" + id + '\'' +
                ", userName='" + userName + '\'' +
                ", age=" + age +
                ", createTime=" + createTime +
                ", tags=" + tags +
                '}';
    }
}