package com.empirefree.huyuqiao.mapper;

/**
 * @program: springboot-sharding
 * @description: mybatilplus的mapper接口
 * @author: huyuqiao
 * @create: 2021/06/19 16:19
 */

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.empirefree.huyuqiao.domain.UserMybatisDO;

public interface UserMybatisPlusMapper extends BaseMapper<UserMybatisDO> {
}
