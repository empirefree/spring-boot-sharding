package com.empirefree.huyuqiao.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: springboot-sharding
 * @description: 全局异常处理
 * @author: huyuqiao
 * @create: 2021/06/19 16:16
 */

@ControllerAdvice
public class GlobalExceptions {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Object handle(Exception ex) {
        logger.error("「 全局异常 」 ===============》{}", ex);
        return "「 全局异常 」错误信息:"+ex.getMessage();
    }
}