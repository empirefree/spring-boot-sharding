package com.empirefree.huyuqiao.service;

import com.empirefree.huyuqiao.domain.UserDO;
import com.empirefree.huyuqiao.domain.UserMybatisDO;

import java.util.List;

/**
 * @program: springboot-sharding
 * @description: 用户service
 * @author: huyuqiao
 * @create: 2021/06/19 16:20
 */

public interface UserService {
    //mybatis 查询
    List<UserDO> all();

    //mybatisplus 查询
    List<UserMybatisDO> allMybatisPlus();

    //添加
    Integer add(UserDO userDO);

    //更新
    Integer update(UserDO userDO);

    //删除
    Integer delete(String id);
}
